#!/bin/sh

_IMAGE="$1"
_TAG="$2"
_DOCKERFILE="$3"
_IMAGE_FILE="$4"

docker pull "$_IMAGE:$_TAG_BEFORE" || true \
&& docker build -t "$_IMAGE:$_TAG" -f "$_DOCKERFILE" . \
&& docker push "$_IMAGE:$_TAG" \
&& echo "$_IMAGE:$_TAG" > "$_IMAGE_FILE"
