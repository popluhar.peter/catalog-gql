FROM node:alpine

ARG MONGODB_URI
ENV MONGODB_URI $MONGODB_URI

ARG MONGODB_DB
ENV MONGODB_DB $MONGODB_DB

ARG MONGO_DB_COLLECTION
ENV MONGO_DB_COLLECTION $MONGO_DB_COLLECTION

ARG APPLICATION_SECRET
ENV APPLICATION_SECRET $APPLICATION_SECRET

ARG USER_NAME
ENV USER_NAME $USER_NAME

ARG USER_PASSWORD
ENV USER_PASSWORD $USER_PASSWORD

ARG COOKIE_NAME
ENV COOKIE_NAME $COOKIE_NAME

ARG TOKEN
ENV TOKEN $TOKEN

ENV NEXT_TELEMETRY_DISABLED=1

WORKDIR /app

COPY client/package.json .
COPY client/package-lock.json .

RUN npm install

COPY client/components components
COPY client/context context
COPY client/copy copy
COPY client/hooks hooks
COPY client/pages pages
COPY client/public public
COPY client/styles styles
COPY client/tests tests
COPY client/types types
COPY client/util util
COPY client/.babelrc .
COPY client/.eslintrc.js .
COPY client/.npmrc .
COPY client/.prettierignore .
COPY client/.prettierrc.json .
COPY client/jest.config.js .
COPY client/jest.setup.js .
COPY client/next-env.d.ts .
COPY client/tsconfig.json .

RUN npm run build

CMD ["npm", "run", "start"]
